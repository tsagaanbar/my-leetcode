## Problems

| 编号 | 题目 | 代码 | 链接 |
| --- | --- | --- | ---  |
| 1003. |  Find Common Characters (查找共用字符) | 📄 [Code](./1003/) | [🌐 Leetcode CN][1003] |
| 1889. |  Minimum Space Wasted From Packaging (装包裹的最小浪费空间) | 📄 [Code](./1889/) | [🌐 Leetcode CN][1889] |

[1003]: https://leetcode.cn/problems/find-common-characters/
[1889]: https://leetcode.cn/problems/minimum-space-wasted-from-packaging/
