#include <vector>
#include <string>
#include <array>

class SolutionA {
public:
    static std::vector<std::string> commonChars(std::vector<std::string>& words) {
        static constexpr int ALPHA_NUM = 26;
        std::array<std::vector<int>, ALPHA_NUM> count;
        const auto len = words.size();
        for (auto & vec : count) {
            vec.resize(len);
        }
        for (int i = 0; i < len; i += 1) {
            for (const auto c: words[i]) {
                count[c - 'a'][i] += 1;
            }
        }

        std::vector<std::string> result;
        for (int i = 0; i < count.size(); i += 1) {
            auto n = *std::min_element(count[i].begin(), count[i].end());
            for (int j = 0; j < n; j += 1) {
                result.emplace_back(1, 'a' + i);
            }
        }

        return result;
    }
};

class SolutionB {
public:
    static std::vector<std::string> commonChars(std::vector<std::string>& words) {
        const int ALPHA_NUM = 26;
        using CharCount = std::array<int, ALPHA_NUM>;
        CharCount minCount;
        minCount.fill(10000);

        for (auto const & word: words) {
            CharCount current;
            current.fill(0);
            for (const auto c : word) {
                current[c - 'a'] += 1;
            }
            for (int i = 0; i < ALPHA_NUM; i += 1) {
                minCount[i] = std::min(current[i], minCount[i]);
            }
        }

        std::vector<std::string> result;
        for (int i = 0; i < ALPHA_NUM; i += 1) {
            for (int j = 0; j < minCount[i]; j += 1) {
                result.emplace_back(1, 'a' + i);
            }
        }
        return result;
    }
};

#include <fmt/core.h>
#include <fmt/ranges.h>

int main() {
    using Solution = SolutionA;
    std::vector<std::string> words = {"bella","label","roller"};
    fmt::print("{}\n", words);
    fmt::print("{}\n", Solution::commonChars(words));
    return 0;
}
